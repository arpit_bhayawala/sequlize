const Product = require('../models/product');

// Get Add Product
exports.getAddProduct = (req, res, next) => {
    res.render('admin/edit-product', { 
    pageTitle: 'Add Product', 
    path: '/admin/add-product',
    editing: false
    });
};

// Post Add Product
exports.postAddProduct =(req, res, next) => {   
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const description = req.body.description;
    const price = req.body.price;
    const product = new Product(null,title,imageUrl,description,price);
    req.user.createProduct({
        title : title,
        price : price,
        imageUrl : imageUrl,
        description : description,
        // userId : req.user.id
    })    
    .then(result => {
        console.log("Created Product");
        res.redirect('/admin/products');
    })
    .catch(err => {
        console.log(err);
        
    });
    
};

// Get Edit Product
exports.getEditProduct = (req, res, next) => {
        const editMode = req.query.edit;
        if(!editMode){
        return res.redirect('/')
        }
        const prodId =  req.params.productId;
        req.user
            .getProducts({where : {id : prodId} })
        // Product.findByPk(prodId)
            .then(products => {
                const product = products[0]; 
                    if(!product){
                        return res.redirect('/');
                    }
                    res.render('admin/edit-product', { 
                    pageTitle: 'Edit Product', 
                    path: '/admin/edit-product',
                    editing:editMode,
                    product:product 
                });
            })
            .catch(err => console.log(err));
}; 

//Post Edit Product
exports.postEditProduct = (req, res, next) => {
    const prodId = req.body.productId;
    const updatedtitle = req.body.title;
    const updatedImageUrl = req.body.imageUrl;
    const updatedDesc = req.body.description;
    const updatedprice = req.body.price
    Product.findByPk(prodId)
    .then(product => {
        product.title = updatedtitle;
        product.price = updatedprice;
        product.description = updatedDesc;
        product.imageUrl = updatedImageUrl;
        return product.save();
    })
    .then(result => {
        console.log('UPDATED PRODUCT');
        res.redirect('/admin/products');
    })
    .catch(err => console.log(err));
};

// Get Products
exports.getProducts =  (req, res, next) => { 
    req.user
    .getProducts()
    .then(products=>{
        res.render('admin/products', {
            prods: products,
            pageTitle: 'Admin Products',
            path: '/admin/products',
        });
    })
    .catch(err => console.log(err));
    
  };

  //Post Delete Product
  exports.postDeleteProduct = (req,res,next) =>{
    const prodId = req.body.productId;
    Product.findByPk(prodId)
    .then(product => {
       return product.destroy()
    })
    .then(result => {
        console.log("Destroyed Product");
        res.redirect('/admin/products');
    })
    .catch(err => console.log(err));
  };